package com.application.android.model.domainalerts

data class Type(
    val domainPurpose: String?,
    val id: Int?,
    val image: String?,
    val imageContentType: String?,
    val owner: Owner?,
    val type: String?
)