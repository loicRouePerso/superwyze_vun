package com.application.android.model.domainalerts

data class Owner(
    val employeePlayerId: List<EmployeePlayerId>,
    val id: Int?,
    val installer: Boolean?,
    val jhiUser: JhiUser?,
    val organization: Organization?
)