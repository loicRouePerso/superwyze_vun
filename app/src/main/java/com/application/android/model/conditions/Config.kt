package com.application.android.model.conditions

import java.io.Serializable

class Config : Serializable {
    var description: String = ""
    val id: Int = 0
    val metadata: String = ""
    val owner: Organization? = null
    val sensor: Sensor? = null
    val type: Type? = null
}

