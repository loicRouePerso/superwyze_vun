package com.application.android.model.domainalerts

import java.io.Serializable

class AlertConfig : Serializable {
    val config: Config? = null
    val id: Int = 0
    var inverse: Boolean = false
    var maximum: Int? = null
    var minimum: Int? = null
    val owner: Owner? = null
    var timestamp: Long? = null
}

