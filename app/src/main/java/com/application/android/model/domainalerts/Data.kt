package com.application.android.model.domainalerts

import java.io.Serializable

class Data : Serializable{
    val alertConfig: AlertConfig? = null
    val id: Int? = null
    val timestamp: Long = 0
    val value: Int? = null
}

