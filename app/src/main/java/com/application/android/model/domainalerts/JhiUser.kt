package com.application.android.model.domainalerts

data class JhiUser(
    val activated: Boolean?,
    val email: String?,
    val firstName: String?,
    val id: Int?,
    val imageUrl: String?,
    val langKey: String?,
    val lastName: String?,
    val login: String?,
    val resetDate: String?
)