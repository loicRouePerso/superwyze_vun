package com.application.android.model.domainalerts

data class OwnerX(
    val id: Int?,
    val kitId: Int?,
    val name: String?
)