package com.application.android.model.domainalerts

data class Sensor(
    val apiKey: String?,
    val description: String?,
    val id: Int?,
    val idDT: String?,
    val owner: Owner?
)