package com.application.android.model.domainalerts

import com.application.android.model.conditions.Employee

data class EmployeePlayerId(
    val employee: Employee?,
    val id: Int?,
    val playerId: String?
)