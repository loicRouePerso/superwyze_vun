package com.application.android.model.conditions

import java.io.Serializable

class Type : Serializable {
    val domainPurpose: String = ""
    val id: Int = 0
    val owner: Organization? = null
    val type: String = ""
    val image : String = ""
    val imageContentType : String = ""
}
