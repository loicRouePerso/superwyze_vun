package com.application.android.model.numberversion

data class NumberVersion(
    val version: String
)