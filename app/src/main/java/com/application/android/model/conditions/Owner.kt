package com.application.android.model.conditions

import java.io.Serializable

class Owner : Serializable {
    val employeePlayerId: List<EmployeePlayerId>? = null
    val id: Int = 0
    val installer: Boolean = false
    val jhiUser: JhiUser? = null
    val organization: Organization? = null
}

