package com.application.android.model.conditions

import java.io.Serializable

class Sensor : Serializable {
    val apiKey: String = ""
    val description: String = ""
    val id: Int = 0
    val idDT: String = ""
    val owner: Organization? = null
}

