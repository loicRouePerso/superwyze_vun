package com.application.android.model.useridentification


class UserCredentials(username: String?, password: String?, val playerId : String, val rememberMe: Boolean) {

    private val username: String? = username

    val mUsername: String
        get() = username ?: ""

    private val password: String? = password

    val mPassword: String
        get() = password ?: ""

    val isUsernameValid: Boolean
        get() = mUsername.isNotEmpty()

    val isPasswordValid: Boolean
        get() = mPassword.isNotEmpty()
}
