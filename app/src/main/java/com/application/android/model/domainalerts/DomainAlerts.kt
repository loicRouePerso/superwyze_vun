package com.application.android.model.domainalerts

import java.io.Serializable

class DomainAlerts : Serializable {
    val `data`: List<Data>? = null
    val message: String? = null
    val success: Boolean? = null
}

