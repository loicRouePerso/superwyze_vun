package com.application.android.model.domainalerts

data class Organization(
    val id: Int?,
    val kitId: Int?,
    val name: String?
)