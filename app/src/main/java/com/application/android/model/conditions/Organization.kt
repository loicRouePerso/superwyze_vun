package com.application.android.model.conditions

import java.io.Serializable

class Organization : Serializable {
    val id: Int = 0
    val kitId: Int = 0
    val name: String = ""
}
