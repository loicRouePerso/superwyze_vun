package com.application.android.model.conditions

import java.io.Serializable

class JhiUser : Serializable {
    val activated: Boolean = false
    val email: String = ""
    val firstName: String = ""
    val id: Int = 0
    val imageUrl: String? = null
    val langKey: String = ""
    val lastName: String = ""
    val login: String = ""
    val resetDate: String = ""
}

