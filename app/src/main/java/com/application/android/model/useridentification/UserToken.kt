package com.application.android.model.useridentification

import com.google.gson.annotations.SerializedName

//Réponse de l'API
data class UserToken (
        @SerializedName("id_token")
        val idToken : String
)
