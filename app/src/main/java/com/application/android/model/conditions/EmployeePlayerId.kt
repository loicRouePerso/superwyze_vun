package com.application.android.model.conditions

import java.io.Serializable

class EmployeePlayerId : Serializable {
    val employee: Employee? = null
    val id: Int = 0
    val playerId: String = ""
}

