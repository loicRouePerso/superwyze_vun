package com.application.android.utils

import androidx.appcompat.widget.AppCompatImageView
import com.application.android.model.conditions.AlertConfig

interface OnTransitionCalled {

    fun onFollowedAlertConfigSelected(tag: String, sharedImageView: AppCompatImageView, alertConfigImage: AlertConfig?)
    fun onUnFollowedAlertConfigSelected(tag: String, sharedImageView: AppCompatImageView, alertConfigImage: AlertConfig?)
    fun initialize()
    fun onFABAction()

}