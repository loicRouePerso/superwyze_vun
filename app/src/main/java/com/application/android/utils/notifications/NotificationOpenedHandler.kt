package com.application.android.utils.notifications

import android.content.Intent
import com.application.android.alertconfigs.view.AlertConfigsActivity
import com.application.android.app.SuperwyzeApp
import com.onesignal.OSNotificationOpenResult
import com.onesignal.OneSignal

class NotificationOpenedHandler : OneSignal.NotificationOpenedHandler {

    override fun notificationOpened(result: OSNotificationOpenResult) {
        startApp()
    }

    private fun startApp() {
        val intent = Intent(SuperwyzeApp.getAppContext(), AlertConfigsActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
        intent.putExtra("loadInfos", "load")
        SuperwyzeApp.getAppContext()?.startActivity(intent)
    }

}
