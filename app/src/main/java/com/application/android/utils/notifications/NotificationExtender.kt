package com.application.android.utils.notifications

import android.util.Log
import androidx.core.app.NotificationCompat
import com.onesignal.NotificationExtenderService
import com.onesignal.OSNotificationReceivedResult
import android.graphics.drawable.BitmapDrawable
import com.application.android.R

class NotificationExtender : NotificationExtenderService() {
    override fun onNotificationProcessing(receivedResult: OSNotificationReceivedResult): Boolean {
        val overrideSettings = OverrideSettings()
        val superwyzeIconDrawable = resources.getDrawable( R.drawable.superwyze_blanc_hd)
        val superwyzeIconBitmap = (superwyzeIconDrawable as BitmapDrawable).bitmap
        val blueColor = resources.getColor(R.color.colorPrimaryBlue)
        overrideSettings.extender = NotificationCompat.Extender { builder ->
            builder.setSmallIcon(R.drawable.logo_image)
            builder.setLargeIcon(superwyzeIconBitmap)
            builder.color = blueColor
            builder.setContentTitle("Superwyze")

        }
        val displayedResult = displayNotification(overrideSettings)
        Log.d("OneSignalExample", "Notification displayed with id: " + displayedResult.androidNotificationId)
        return true
    }
}
