package com.application.android.utils.notifications

import com.onesignal.OSNotification
import com.onesignal.OneSignal
import timber.log.Timber

class NotificationReceivedHandler : OneSignal.NotificationReceivedHandler {

    override fun notificationReceived(notification: OSNotification) {

        val data = notification.payload?.additionalData
        val customKey: String
        if (data != null) {
            customKey = data.optString("customkey", null)

            if (customKey != null) {
                Timber.tag("OneSignalExample").i("customkey set with value: %s", customKey)
            }

        }
    }
}