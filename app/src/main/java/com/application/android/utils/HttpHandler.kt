package com.application.android.utils

import android.util.Log
import timber.log.Timber
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.ProtocolException
import java.net.URL

internal class HttpHandler {

    fun makeServiceCall(reqUrl: String): String? {
        var response: String? = null
        try {
            val url = URL(reqUrl)
            val conn = url.openConnection() as HttpURLConnection
            conn.requestMethod = "GET"
            // read the response
            val `in` = BufferedInputStream(conn.inputStream)
            response = convertStreamToString(`in`)
        } catch (e: MalformedURLException) {
            Timber.e(
                "MalformedURL" +
                        "Exception: " + e.message
            )
        } catch (e: ProtocolException) {
            Timber.e("ProtocolException: " + e.message)
        } catch (e: IOException) {
            Timber.e("IOException: " + e.message)
        } catch (e: Exception) {
            Timber.e("Exception: " + e.message)
        }

        return response
    }

    private fun convertStreamToString(`is`: InputStream): String {
        val reader = BufferedReader(InputStreamReader(`is`))
        val sb = StringBuilder()

        var line: String? = null
        try {
            while ({line = reader.readLine(); line}() != null) {
                sb.append(line).append('\n')
            }
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            try {
                `is`.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        return sb.toString()
    }

    companion object {

        private val TAG = HttpHandler::class.java.simpleName
    }


}
