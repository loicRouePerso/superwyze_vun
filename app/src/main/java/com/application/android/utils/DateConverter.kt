package com.application.android.utils

import java.sql.Date
import java.text.SimpleDateFormat
import java.util.*


object DateConverter {

    const val format = "dd/MM/yyyy HH:mm"
    private val simpleDateFormat = SimpleDateFormat(format, Locale.FRANCE)

    fun convertMillisecondToDate(timestamp: Long?): String {
        timestamp?.let {
            return simpleDateFormat.format(Date(timestamp))
        }
        return ""
    }

    fun get3DaysAgoDate(): Long {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, -3)
        return calendar.timeInMillis
    }

    fun compareDates(date: Long, threeDaysAgoDate: Long) : Boolean{
        val compare = date.compareTo(threeDaysAgoDate)
        return compare > 0
    }
}