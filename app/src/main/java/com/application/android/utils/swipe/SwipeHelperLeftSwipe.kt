package com.application.android.utils.swipe

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Point
import android.graphics.Rect
import android.graphics.RectF
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import java.util.ArrayList
import java.util.HashMap
import java.util.LinkedList
import java.util.Queue
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.DisplayMetrics
import androidx.core.content.res.ResourcesCompat

abstract class SwipeHelperLeftSwipe(context: Context?, private val recyclerView: RecyclerView) :
    ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

    companion object {
        private const val YOUR_WIDTH_IN_PIXEL_PER_BUTTON = 300f
        const val BUTTON_WIDTH = YOUR_WIDTH_IN_PIXEL_PER_BUTTON
    }

    private var buttons: MutableList<UnderlayButton>? = null
    private lateinit var gestureDetector: GestureDetector
    private var swipedPos = -1
    private var swipeThreshold = 0.2f
    private var buttonsBuffer: MutableMap<Int, MutableList<UnderlayButton>>
    private lateinit var recoverQueue: Queue<Int>

    private val gestureListener = object : GestureDetector.SimpleOnGestureListener() {
        override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
            for (button in buttons!!) {
                if (button.onClick(e.x, e.y))
                    break
            }
            return true
        }
    }

    private val onTouchListener = View.OnTouchListener { _, motionE ->
        if (swipedPos < 0) return@OnTouchListener false
        val point = Point(motionE.rawX.toInt(), motionE.rawY.toInt())

        val swipedViewHolder = recyclerView.findViewHolderForAdapterPosition(swipedPos)
        val swipedItem = swipedViewHolder?.itemView
        val rect = Rect()
        swipedItem?.getGlobalVisibleRect(rect)

        if (motionE.action == MotionEvent.ACTION_DOWN || motionE.action == MotionEvent.ACTION_UP || motionE.action == MotionEvent.ACTION_MOVE) {
            if (rect.top < point.y && rect.bottom > point.y)
                gestureDetector.onTouchEvent(motionE)
            else {
                recoverQueue.add(swipedPos)
                swipedPos = -1
                recoverSwipedItem()
            }
        }
        false
    }

    init {
        this.buttons = ArrayList()
        this.gestureDetector = GestureDetector(context, gestureListener)
        this.recyclerView.setOnTouchListener(onTouchListener)
        buttonsBuffer = HashMap()
        recoverQueue = object : LinkedList<Int>() {
            override fun add(element: Int): Boolean {
                return if (contains(element))
                    false
                else
                    super.add(element)
            }
        }
        attachSwipe()
    }

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val pos = viewHolder.adapterPosition

        if (swipedPos != pos)
            recoverQueue.add(swipedPos)

        swipedPos = pos

        if (buttonsBuffer.containsKey(swipedPos)) buttons = buttonsBuffer[swipedPos]
        else buttons!!.clear()

        buttonsBuffer.clear()
        swipeThreshold = 0.2f * buttons!!.size.toFloat() * BUTTON_WIDTH
        recoverSwipedItem()
    }

    override fun getSwipeThreshold(viewHolder: RecyclerView.ViewHolder): Float {
        return swipeThreshold
    }

    override fun getSwipeEscapeVelocity(defaultValue: Float): Float {
        return 0.1f * defaultValue
    }

    override fun getSwipeVelocityThreshold(defaultValue: Float): Float {
        return 3.0f * defaultValue
    }

    override fun onChildDraw(
        canvas: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        val position = viewHolder.adapterPosition
        var translationX = dX
        val itemView = viewHolder.itemView

        if (position < 0) {
            swipedPos = position
            return
        }

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            if (dX < 0) {
                var buffer: MutableList<UnderlayButton>? = ArrayList()

                if (!buttonsBuffer.containsKey(position)) {
                    buffer?.let { instantiateUnderlayButton(viewHolder, it) }
                    buttonsBuffer[position] = buffer!!
                } else {
                    buffer = buttonsBuffer[position]
                }

                translationX = dX * buffer!!.size.toFloat() * BUTTON_WIDTH / itemView.width
                drawButtons(canvas, viewHolder, buffer, position, translationX)
            }
        }

        super.onChildDraw(canvas, recyclerView, viewHolder, translationX, dY, actionState, isCurrentlyActive)
    }

    @Synchronized
    private fun recoverSwipedItem() {
        while (!recoverQueue.isEmpty()) {
            val position = recoverQueue.poll()
            if (position > -1) {
                recyclerView.adapter!!.notifyItemChanged(position)
            }
        }
    }

    private fun drawButtons(canvas: Canvas, viewHolder: RecyclerView.ViewHolder, buffer: MutableList<UnderlayButton>, position: Int, dX: Float) {
        val buttonWidthWithoutPadding = 20

        val itemView = viewHolder.itemView
        var right = itemView.right.toFloat()
        val dButtonWidth = (-1.0).toFloat() * dX / buffer.size

        for (button in buffer) {
            val left = right - dButtonWidth

            button.onDraw(
                canvas,
                RectF(left + buttonWidthWithoutPadding, itemView.top.toFloat(), right - buttonWidthWithoutPadding, itemView.bottom.toFloat()),
                position,
                itemView
            )
            right = left + buttonWidthWithoutPadding
        }
    }

    private fun attachSwipe() {
        val itemTouchHelper = ItemTouchHelper(this)
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    abstract fun instantiateUnderlayButton(viewHolder: RecyclerView.ViewHolder, underlayButtons: MutableList<UnderlayButton>)

    class UnderlayButton(
        val context: Context,
        private val text: String,
        imageResId: Int,
        private val color: Int,
        private val clickListener: UnderlayButtonClickListener
    ) {

        private var pos: Int = 0
        private lateinit var clickRegion: RectF
        private val resource: Resources = context.resources
        var drawable = ResourcesCompat.getDrawable(resource, imageResId, null)!!

        private val icon = drawableToBitmap(drawable)

        fun onClick(x: Float, y: Float): Boolean {
            if (clickRegion.contains(x, y)) {
                clickListener.onClick(pos)
                return true
            }
            return false
        }

        fun onDraw(canvas: Canvas, rect: RectF, position: Int, itemView: View) {
            val paint = Paint()
            val corners = 16f
            // Draw background
            paint.color = color
            //canvas.drawRect(rect, paint)
            canvas.drawRoundRect(rect, corners, corners, paint)
            // Draw Text
            paint.color = Color.WHITE
            paint.textSize = 35.toFloat()
            val rectF = Rect()
            val cHeight = rect.height()
            val cWidth = rect.width()
            paint.textAlign = Paint.Align.LEFT
            paint.getTextBounds(text, 0, text.length, rectF)
            val x = cWidth / 2f - rectF.width() / 2f - rectF.left.toFloat()
            val y = cHeight / 2f + rectF.height() / 2f - rectF.bottom
            canvas.drawText(text, rect.left + x, rect.top + y, paint)

            //Draw Image
            canvas.drawBitmap(
                icon,
                itemView.right.toFloat() - convertDpToPx(34) - icon.width,
                itemView.top.toFloat() + (itemView.bottom.toFloat() - itemView.top.toFloat() - icon.height) / 2,
                paint
            )
            clickRegion = rect
            this.pos = position
        }

        private fun convertDpToPx(dp: Int): Int {
            return Math.round(dp * (resource.displayMetrics?.xdpi!! / DisplayMetrics.DENSITY_DEFAULT))
        }

        private fun drawableToBitmap(drawable: Drawable): Bitmap {
            if (drawable is BitmapDrawable) {
                return drawable.bitmap
            }
            val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            drawable.setBounds(0, 0, canvas.width, canvas.height)
            drawable.draw(canvas)
            return bitmap
        }
    }

    interface UnderlayButtonClickListener {
        fun onClick(pos: Int)
    }
}