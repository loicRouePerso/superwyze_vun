package com.application.android.login.view

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import br.com.simplepass.loadingbutton.animatedDrawables.ProgressType
import com.application.android.alertconfigs.view.AlertConfigsActivity
import com.application.android.app.AuthenticationPrefs
import com.application.android.login.LoginViewModel
import com.application.android.utils.BASE_URL_SUPERWYZE_COUNT
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*
import com.application.android.R
import com.application.android.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity() {

    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (AuthenticationPrefs.getLoggedStatus()) {
            val intent = Intent(this, AlertConfigsActivity::class.java)
            startActivity(intent)
        } else {
            setBinding()
            resetPassword()
            checkConnectivity()
            onLoadingButtonClickListener()
        }
    }

    private fun setBinding() {
        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        val binding = DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this
    }

    private fun onLoadingButtonClickListener() {
        login_button_loading.run {
            setOnClickListener {
                viewModel.onLoginClicked()
                progressType = ProgressType.INDETERMINATE
                viewModel.loadingButtonVisibility.observe(this@LoginActivity, Observer {
                    startAnimation()
                    //progressAnimator(this).start()
                })
                viewModel.disableLoadingButtonVisibility.observe(this@LoginActivity, Observer {
                    doneLoadingAnimation(defaultColor(context), defaultDoneImage(context.resources))
                })
                viewModel.navigateToMain.observe(this@LoginActivity, Observer {
                    val intent = Intent(this@LoginActivity, AlertConfigsActivity::class.java)
                    startActivity(intent)
                    finish()
                })
                viewModel.loginFailed.observe(this@LoginActivity, Observer {
                    postDelayed({
                        doneLoadingAnimation(defaultColor(context), defaultErrorImage(context.resources))
                    }, 1000)
                    postDelayed({ revertAnimation() }, 3500)
                    hideKeyboard()
                    val snackBar =
                        Snackbar.make(constraint_layout_login_activity, context.getString(R.string.wrong_login_password), Snackbar.LENGTH_LONG)
                    val view = snackBar.view
                    val tv = view.findViewById<TextView>(R.id.snackbar_text)
                    tv.setTextColor(Color.RED)
                    snackBar.show()

                })

            }
        }
    }

    private fun defaultColor(context: Context) = ContextCompat.getColor(context, R.color.colorSecondaryBlue)

    private fun defaultDoneImage(resources: Resources) =
        BitmapFactory.decodeResource(resources, R.drawable.ic_check)

    private fun defaultErrorImage(resources: Resources) =
        BitmapFactory.decodeResource(resources, R.drawable.error)

    private fun Activity.hideKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(findViewById<View>(android.R.id.content).windowToken, 0);
    }

    private fun resetPassword() {
        viewModel.openResetPasswordPage.observe(this, Observer {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(BASE_URL_SUPERWYZE_COUNT))
            startActivity(intent)
        })
    }

    private fun checkConnectivity() {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        val isConnected = activeNetwork != null && activeNetwork.isConnected
        if (!isConnected) {
            Toast.makeText(this, getString(R.string.check_connectivity), Toast.LENGTH_SHORT).show()
        }
    }
}

