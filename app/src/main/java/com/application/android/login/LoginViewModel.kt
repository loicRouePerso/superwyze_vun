package com.application.android.login

import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.application.android.utils.SingleLiveEvent
import com.application.android.app.AuthenticationPrefs
import com.application.android.model.useridentification.UserCredentials
import com.application.android.api.RetrofitClientProvider
import com.application.android.app.SuperwyzeApp
import com.onesignal.OneSignal
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import com.application.android.R

class LoginViewModel : ViewModel() {

    val loginContent = MutableLiveData<String>()
    val passwordContent = MutableLiveData<String>()

    var errorUsername = MutableLiveData<String>()
    var errorPassword = MutableLiveData<String>()

    private val _loadingButtonVisibility = SingleLiveEvent<Any>()
    val loadingButtonVisibility : LiveData<Any>
        get() = _loadingButtonVisibility

    private val _disableLoadingButtonVisibility = SingleLiveEvent<Any>()
    val disableLoadingButtonVisibility : LiveData<Any>
        get() = _disableLoadingButtonVisibility

    private val _navigateToMain = SingleLiveEvent<Any>()
    val navigateToMain: LiveData<Any>
        get() = _navigateToMain

    private val _openResetPasswordPage = SingleLiveEvent<Any>()
    val openResetPasswordPage: LiveData<Any>
        get() = _openResetPasswordPage

    private val _loginFailed = SingleLiveEvent<Int>()
    val loginFailed: LiveData<Int>
        get() = _loginFailed

    fun onLoginClicked() {
        Handler().run {
            val userId = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId
            val user = UserCredentials(loginContent.value, passwordContent.value, userId, true )

            if (!user.isUsernameValid || !user.isPasswordValid) {
                if (!user.isUsernameValid) {
                    errorUsername.setValue(SuperwyzeApp.getAppContext()?.getString(R.string.enter_email_or_login))
                } else {
                    errorUsername.setValue(null)
                }
                if (!user.isPasswordValid){
                    errorPassword.setValue(SuperwyzeApp.getAppContext()?.getString(R.string.enter_password))
                }
                else {
                    errorPassword.setValue(null)
                }
            } else {
                _loadingButtonVisibility.call()
                RetrofitClientProvider.provideSuperwyzeApi()
                        .postUser(user, userId)
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeBy(
                                onNext = { accessToken ->
                                    _disableLoadingButtonVisibility.call()
                                    AuthenticationPrefs.setLoggedIn()
                                    AuthenticationPrefs.saveLogin(user.mUsername)
                                    AuthenticationPrefs.saveAuthToken(accessToken.idToken)
                                    println(accessToken.idToken)
                                   _navigateToMain.call()
                                },
                                onError = { e ->
                                    _loginFailed.call()
                                    e.printStackTrace()
                                }
                        )
            }
        }
    }

    fun forgotPassword() {
        _openResetPasswordPage.call()
    }
}








