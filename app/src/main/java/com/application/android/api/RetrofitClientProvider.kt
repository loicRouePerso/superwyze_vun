package com.application.android.api

import com.application.android.utils.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitClientProvider {

    private var retrofit: Retrofit

    init {
        retrofit =
            initRetrofitClient(BASE_URL)
    }

    private fun initRetrofitClient(baseUrl: String): Retrofit {
            val interceptor = HttpLoggingInterceptor()
            val interceptorHeader = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            interceptorHeader.level = HttpLoggingInterceptor.Level.HEADERS
            val client = OkHttpClient.Builder()
                    .addNetworkInterceptor(SupportInterceptor())
                    .addNetworkInterceptor(interceptor)
                    .addNetworkInterceptor(interceptorHeader)
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100, TimeUnit.SECONDS)
                    .build()

        return Retrofit.Builder()
                .client(client)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    fun provideSuperwyzeApi() : APIService = retrofit.create(APIService::class.java)
}

