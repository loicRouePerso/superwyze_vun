package com.application.android.api

import com.application.android.model.conditions.AlertConfig
import com.application.android.model.conditions.Config
import com.application.android.model.domainalerts.Data
import com.application.android.model.numberversion.NumberVersion
import com.application.android.model.useridentification.UserCredentials
import com.application.android.model.useridentification.UserToken
import io.reactivex.Observable
import retrofit2.http.*

interface APIService {

    @POST("/api/v1/authenticate")
    fun postUser(@Body userCredentials: UserCredentials, @Header("playerId") playerId: String): Observable<UserToken>

    @GET("api/v1/myAlertConfigurations")
    fun getAllAlertConfigurations(@Header("Authorization") token: String?, @Header("playerId") playerId: String): Observable<List<AlertConfig>>

    @PUT("/api/v1/myAlertConfigurations")
    fun putNewAlertConfigParameters(@Header("Authorization") token: String?, @Header("playerId") playerId: String, @Body alertConfig: AlertConfig?): Observable<AlertConfig>

    @GET("/api/v1/myAlertConfigurations/{id}/history/reset")
    fun resetCondition(@Header("Authorization") token: String?,@Header("playerId") playerId: String, @Path("id") id: Int): Observable<AlertConfig>

    @PUT("/api/v1/mySensorDtDomainTypes")
    fun modifySensorDtDomainType(@Header("Authorization") token: String?, @Header("playerId") playerId: String, @Body config: Config?): Observable<Config>

    @GET("/api/v1/myDomainAlerts")
    fun getAllDomainAlerts(@Header("Authorization") token : String, @Header("playerId") playerId : String) : Observable<List<Data>>

    @GET("https://bridge.buddyweb.fr/api/superwyzeversion/version")
    fun getNumberVersion() : Observable<List<NumberVersion>>

}


