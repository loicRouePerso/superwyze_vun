package com.application.android.alertconfigs.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.transition.TransitionInflater
import com.application.android.BuildConfig
import com.application.android.R
import com.application.android.alertconfigs.AlertConfigsViewModel
import com.application.android.alertconfigs.view.alertconfigedition.AlertConfigEditionFragment
import com.application.android.alertconfigs.view.followedalertconfigs.view.FollowedAlertConfigsFragment
import com.application.android.alertconfigs.view.unfollowedalertconfigs.view.UnfollowedAlertConfigsFragment
import com.application.android.app.AuthenticationPrefs
import com.application.android.login.view.LoginActivity
import com.application.android.model.conditions.AlertConfig
import com.application.android.settings.SettingsActivity
import com.application.android.utils.OnTransitionCalled
import kotlinx.android.synthetic.main.activity_alert_configs.*
import kotlinx.android.synthetic.main.fragment_followed_alert_configs.*

class AlertConfigsActivity : AppCompatActivity(), OnTransitionCalled {

    private lateinit var viewModel: AlertConfigsViewModel

    companion object {
        private var back_pressed: Long = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alert_configs)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, FollowedAlertConfigsFragment.newInstance(), "followedAlertConfigList")
                .commit()
        }

        if (!AuthenticationPrefs.getLoggedStatus()) {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
        viewModel = ViewModelProviders.of(this).get(AlertConfigsViewModel::class.java)
        checkNumberVersion()
        setToolbar()
    }

    private fun checkNumberVersion() {
        val versionName = BuildConfig.VERSION_NAME
        viewModel.numberVersionMLD.observe(this, Observer { it ->
            it.forEach {
                if (it.version == versionName) {

                } else {
                    val builder = AlertDialog.Builder(this@AlertConfigsActivity)
                    builder.setTitle(getString(R.string.new_version_title))
//                builder.setIcon(R.mipmap.ic_launcher)
                    builder.setCancelable(false)
                    builder.setMessage(getString(R.string.new_version))
                        .setPositiveButton(getString(R.string.MAJ)) { _, _ ->
                            val appName = packageName
                            try {
                                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appName")))
                            } catch (anfe: android.content.ActivityNotFoundException) {
                                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=$appName")))
                            }
                            finish()
                        }.setNegativeButton("Une prochaine fois") { _, _ ->
                            closeContextMenu()
                        }
                    val alert = builder.create()
                    alert.show()
                }
            }
        })
    }

    override fun initialize() {
        displayFollowedAlertConfigToolbar()
    }

    override fun onUnFollowedAlertConfigSelected(tag: String, sharedImageView: AppCompatImageView, alertConfigImage: AlertConfig?) {
        val alertConfigurationFragment =
            AlertConfigEditionFragment.newInstance(false, ViewCompat.getTransitionName(sharedImageView), alertConfigImage)
        alertConfigurationFragment.sharedElementEnterTransition = TransitionInflater.from(this).inflateTransition(R.transition.default_transition)
        alertConfigurationFragment.exitTransition = TransitionInflater.from(this).inflateTransition(R.transition.default_transition)
        ViewCompat.getTransitionName(sharedImageView)?.let {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, alertConfigurationFragment)
                .addSharedElement(sharedImageView, it)
                .setReorderingAllowed(true)
                .addToBackStack(tag)
                .commit()
        }
        displayAlertConfigurationEditionToolbar()
    }

    override fun onFollowedAlertConfigSelected(tag: String, sharedImageView: AppCompatImageView, alertConfigImage: AlertConfig?) {
        val alertConfigurationFragment =
            AlertConfigEditionFragment.newInstance(true, ViewCompat.getTransitionName(sharedImageView), alertConfigImage)

        alertConfigurationFragment.sharedElementEnterTransition = TransitionInflater.from(this).inflateTransition(R.transition.default_transition)
        alertConfigurationFragment.exitTransition = TransitionInflater.from(this).inflateTransition(R.transition.default_transition)

        ViewCompat.getTransitionName(sharedImageView)?.let {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, alertConfigurationFragment)
                .addSharedElement(sharedImageView, it)
                .setReorderingAllowed(true)
                .addToBackStack(tag)
                .commit()
        }
        displayAlertConfigurationEditionToolbar()
    }

    override fun onFABAction() {
        val unFollowedFragment = UnfollowedAlertConfigsFragment.newInstance()

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, unFollowedFragment, "UnfollowedAlertConfigsFragment")
            .addToBackStack(null)
            .commit()

        displayUnfollowedAlertConfigToolbar()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent?.getStringExtra("loadInfos").equals("load")) {
            viewModel.loadAllAlertConfigs()
        }
    }

    private fun setToolbar() {
        val mToolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(mToolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }
    }

    private fun displayUnfollowedAlertConfigToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.title = getString(R.string.add_condition)
        floating_button.visibility = View.GONE
    }

    private fun displayFollowedAlertConfigToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        toolbar.title = getString(R.string.active_condition_list)
        floating_button.visibility = View.VISIBLE
    }

    private fun displayAlertConfigurationEditionToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.title = "Configuration de l'alerte"
        floating_button.visibility = View.GONE
    }

    private fun doubleClickToExit() {
        if (back_pressed + 2000 > System.currentTimeMillis())
            finish()
        else
            Toast.makeText(this, getString(R.string.click_again_to_exit), Toast.LENGTH_SHORT).show()
        back_pressed = System.currentTimeMillis()
    }

    private fun oneStepBack() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val fragmentManager = supportFragmentManager
        if (fragmentManager.backStackEntryCount >= 2) {
            fragmentManager.popBackStackImmediate()
            fragmentTransaction.commit()
        } else {
            doubleClickToExit()
        }
    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        if (count == 0) {
            oneStepBack()
        } else {
            supportFragmentManager.popBackStack()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_add_condition, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.action_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
                finish()
                return true
            }
            R.id.logout -> {
                AuthenticationPrefs.setLoggedOut()
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}



