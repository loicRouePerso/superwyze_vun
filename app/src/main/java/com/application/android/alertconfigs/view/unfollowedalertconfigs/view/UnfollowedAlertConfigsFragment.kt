package com.application.android.alertconfigs.view.unfollowedalertconfigs.view

import android.content.Context
import android.graphics.Color
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Handler
import android.view.*
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.application.android.model.conditions.AlertConfig
import com.application.android.utils.layout.OnItemClickListener
import com.application.android.utils.layout.addOnItemClickListener
import kotlinx.android.synthetic.main.fragment_unfollowed_alert_configs.*
import com.application.android.alertconfigs.AlertConfigsViewModel
import com.application.android.alertconfigs.view.unfollowedalertconfigs.UnfollowedAlertConfigsAdapter
import com.google.android.material.snackbar.Snackbar
import com.application.android.R
import com.application.android.utils.OnTransitionCalled
import java.lang.ClassCastException

class UnfollowedAlertConfigsFragment : Fragment() {

    private var errorSnackBar: Snackbar? = null
    private lateinit var viewModel: AlertConfigsViewModel
    private lateinit var listener: OnTransitionCalled
    private val errorClickListener = View.OnClickListener { viewModel.loadAllAlertConfigs() }

    companion object {
        fun newInstance() = UnfollowedAlertConfigsFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnTransitionCalled) listener = context
        else throw ClassCastException(context.toString())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_unfollowed_alert_configs, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = activity?.run {
            ViewModelProviders.of(this).get(AlertConfigsViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        add_condition_recycler_view?.layoutManager = LinearLayoutManager(activity)
        swipeRefresh()
        displayUnFollowedCondition()
        followNewCondition()
        observeError()
    }

    private fun displayUnFollowedCondition() {
        viewModel.displayUnFollowedAlertConfigListMLD.observe(this, Observer { conditionList ->
            displayList(conditionList)
        })
    }

    private fun followNewCondition() {
        add_condition_recycler_view.addOnItemClickListener(object : OnItemClickListener {
            override fun onItemClicked(position: Int, view: View) {
                val holder = UnfollowedAlertConfigsAdapter.AddConditionViewHolder(view)
                val image = holder.image
                val alertConfig = viewModel.getUnfollowedCondition(position)
                listener.onUnFollowedAlertConfigSelected("alertConfigEdition", image, alertConfig)
                viewModel.selectAlertConfig(alertConfig)
            }
        })
    }

    private fun displayList(conditionList: MutableList<AlertConfig>) {
        add_condition_recycler_view?.adapter =
            UnfollowedAlertConfigsAdapter(conditionList)

        viewModel.loadingButtonVisibility.observe(this, Observer {
            add_condition_list_progress.visibility = View.VISIBLE
            add_condition_recycler_view.visibility = View.GONE
        })

        viewModel.disableLoadingButtonVisibility.observe(this, Observer {
            add_condition_list_progress.visibility = View.GONE
            add_condition_recycler_view.visibility = View.VISIBLE
        })

        if (conditionList.isEmpty()) {
            add_condition_recycler_view.visibility = View.GONE
            empty_recycler_view_text_add_condition.visibility = View.VISIBLE
        } else {
            add_condition_recycler_view.visibility = View.VISIBLE
            empty_recycler_view_text_add_condition.visibility = View.GONE
        }
    }

    private fun swipeRefresh() {
        add_condition_swipe_refresh?.setOnRefreshListener {
            scheduleRefreshStop()
            viewModel.loadAllAlertConfigs()
        }
    }

    private fun scheduleRefreshStop() {
        val handler = Handler()
        handler.postDelayed({ add_condition_swipe_refresh?.isRefreshing = false }, 2000)
    }

    private fun observeError() {
        viewModel.retrievingHistoryDataErrorMessage.observe(this, Observer { errorMessage ->
            if (errorMessage != null) showError(errorMessage) else hideError()
        })
    }

    private fun showError(@StringRes errorMessage: Int) {
        errorSnackBar = Snackbar.make(unfollowed_alert_config_list_container, errorMessage, Snackbar.LENGTH_LONG)
        errorSnackBar?.setAction(R.string.retry, errorClickListener)
        errorSnackBar?.setActionTextColor(Color.RED)
        errorSnackBar?.show()
    }

    private fun hideError() {
        errorSnackBar?.dismiss()
    }
}
