package com.application.android.alertconfigs.view.unfollowedalertconfigs

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.drawable.GradientDrawable
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.application.android.R
import com.application.android.model.conditions.AlertConfig

class UnfollowedAlertConfigsAdapter(private val unFollowedAlertConfigList: MutableList<AlertConfig>) : RecyclerView.Adapter<UnfollowedAlertConfigsAdapter.AddConditionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddConditionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_unfollowed_alert_config, parent, false)
        return AddConditionViewHolder(view)
    }

    override fun getItemCount(): Int {
        return unFollowedAlertConfigList.size
    }

    override fun onBindViewHolder(holder: AddConditionViewHolder, position: Int) {
        holder.bind(unFollowedAlertConfigList[position])
    }

    class AddConditionViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val title = view.findViewById<TextView>(R.id.unfollowed_alert_config_title)
        private val location = view.findViewById<TextView>(R.id.unfollowed_alert_config_text_location)
        val image: AppCompatImageView = view.findViewById(R.id.unfollowed_alert_config_image)
        private val itemContainer = view.findViewById<ConstraintLayout>(R.id.item_unfollowed_alert_config_container)
        private val context: Context = image.context

        fun bind(addConditionInfo: AlertConfig) {
            val itemBackgroundColor = itemContainer.background as GradientDrawable
            val jpg = addConditionInfo.config?.type?.image
            if (jpg != null){
                val imageBytes = Base64.decode(jpg, 0)
                val imageBitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
                image.setImageBitmap(imageBitmap)

            }
            val colorBlue = ContextCompat.getColor(context, R.color.colorPrimaryBlue)
            itemBackgroundColor.setColor(colorBlue)
            title?.text = addConditionInfo.config?.description
            location?.text = addConditionInfo.config?.metadata

        }
    }
}
