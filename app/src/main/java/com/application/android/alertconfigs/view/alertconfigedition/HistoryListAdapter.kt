package com.application.android.alertconfigs.view.alertconfigedition

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.application.android.R
import com.application.android.app.SuperwyzeApp
import com.application.android.model.domainalerts.Data
import com.application.android.utils.DateConverter

class HistoryListAdapter(private val allHistoryList: MutableList<Data>) : RecyclerView.Adapter<HistoryListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_history, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return allHistoryList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(allHistoryList[position])
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        private val dateTextView = view.findViewById<TextView>(R.id.history_date)
        private val itemBackground = view.findViewById<ConstraintLayout>(R.id.item_history_container)
        private val context: Context? = SuperwyzeApp.getAppContext()

        fun bind(data: Data) {

            val date = DateConverter.convertMillisecondToDate(data.timestamp)
            dateTextView.text = date

            val itemBackgroundColor = itemBackground.background as GradientDrawable
            val colorBlue = context?.let { ContextCompat.getColor(it, R.color.colorPrimaryBlue) }
            colorBlue?.let { itemBackgroundColor.setColor(it) }
        }
    }
}