package com.application.android.alertconfigs.view.alertconfigedition

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.View
import com.application.android.model.conditions.AlertConfig
import kotlinx.android.synthetic.main.fragment_alert_configuration_edition.*
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.TransitionInflater
import com.application.android.R
import com.application.android.alertconfigs.AlertConfigsViewModel
import com.application.android.alertconfigs.view.AlertConfigsActivity
import com.application.android.app.Filter
import com.application.android.model.conditions.Config
import com.application.android.model.domainalerts.Data
import com.application.android.utils.DateConverter


class AlertConfigEditionFragment : Fragment() {

    private lateinit var viewModel: AlertConfigsViewModel
    private var displayHistoryList: MutableList<Data> = arrayListOf()

    private var compactEditionHeight = 0
    internal var expandedEditionHeight = 0
    private var compactHistoryHeight = 0
    internal var expandedHistoryHeight = 0

    companion object {

        private const val EXTRA_TRANSITION_NAME = "EXTRA_TRANSITION_NAME"
        private const val EXTRA_ALERT_CONFIG_SELECTED = "EXTRA_ALERT_CONFIG_SELECTED"
        private const val EXTRA_RECYCLER_VIEW_VISIBILITY = "EXTRA_RECYCLER_VIEW_VISIBILITY"
        private const val rotationDuration = 200L

        fun newInstance(recyclerViewVisibility: Boolean, transitionName: String?, alertConfig: AlertConfig?) = AlertConfigEditionFragment().apply {
            arguments = bundleOf(
                EXTRA_ALERT_CONFIG_SELECTED to alertConfig,
                EXTRA_TRANSITION_NAME to transitionName,
                EXTRA_RECYCLER_VIEW_VISIBILITY to recyclerViewVisibility
            )
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_alert_configuration_edition, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        viewModel = activity?.run {
            ViewModelProviders.of(this).get(AlertConfigsViewModel::class.java)
        } ?: throw Exception("Invalid activity")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        expandEditionCardView()
        expandableCardView()
        expandHistoryCardView()
        onValidateButtonAction()
        displayHistoryList.clear()
        val receivedAlertConfig = arguments?.getSerializable(EXTRA_ALERT_CONFIG_SELECTED) as AlertConfig

        val config = receivedAlertConfig.config
        observeHistoryList(config)
        val jpg = receivedAlertConfig.config?.type?.image
        val title = receivedAlertConfig.config?.description
        val sensorDescription = receivedAlertConfig.config?.sensor?.description
        val localization = receivedAlertConfig.config?.metadata
        val checkState = receivedAlertConfig.inverse
        val transitionName = arguments?.getString(EXTRA_TRANSITION_NAME)

        val imageBytes = Base64.decode(jpg, 0)
        val imageBitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        val drawable = BitmapDrawable(resources, imageBitmap)

        activity_add_condition_image.transitionName = transitionName
        activity_add_condition_image.setImageDrawable(drawable)
        textView_condition_type.text = title
        alert_config_used_sensor?.text = sensorDescription
        edition_activity_localization?.text = localization
        condition_triggered_switch.isChecked = checkState
        if (checkState) condition_triggered_switch.text = getString(R.string.open_sensor)
        else condition_triggered_switch.text = getString(R.string.closed_sensor)

        receivedAlertConfig.minimum?.let { range_bar.getThumb(0).setValue(it) }
        receivedAlertConfig.maximum?.let { range_bar.getThumb(1).setValue(it) }

        when (receivedAlertConfig.config?.type?.type) {
            "TOUCH" -> {
                touchElementsVisibility()
            }
            "PROXIMITY" -> {
                proximityElementsVisibility()
            }
            "TEMPERATURE" -> {
                temperatureElementsVisibility()
            }
        }
    }

    private fun observeHistoryList(receivedAlertConfig: Config?) {
        val recyclerViewVisibility = arguments?.getBoolean(EXTRA_RECYCLER_VIEW_VISIBILITY)
        val threeDaysBeforeDate = DateConverter.get3DaysAgoDate()
        if (recyclerViewVisibility!!) {
            viewModel.displayedHistoryListMLD.observe(this, Observer { dataList ->
                displayHistoryList.clear()
                dataList.forEach {
                    if (it.alertConfig?.config?.sensor?.id == receivedAlertConfig?.sensor?.id) {
                        val superior = DateConverter.compareDates(it.timestamp, threeDaysBeforeDate)
                        if (superior) {
                            displayHistoryList.add(it)
                        }
                    }
                }
                alert_config_history_list_recycler_view?.layoutManager = LinearLayoutManager(context)
                if (displayHistoryList.size == 0) {
                    alert_config_edition_empty_history_textview.visibility = View.VISIBLE
                    alert_config_history_list_recycler_view.visibility = View.GONE
                } else {
                    alert_config_edition_empty_history_textview.visibility = View.INVISIBLE
                    alert_config_history_list_recycler_view.visibility = View.VISIBLE
                    alert_config_history_list_recycler_view.adapter = HistoryListAdapter(displayHistoryList)
                    val displayHistoryListSize = displayHistoryList.size.toString()
                    alert_configuration_history_number.text = displayHistoryListSize
                }
            })
        } else {
            cardView_history_container.visibility = View.GONE
        }
    }

    private fun onValidateButtonAction() {
        var formerConfig: AlertConfig? = null
        var formerDescription: String? = ""
        var formerState: Boolean? = null
        var formerMaximum: Int? = null
        var formerMinimum: Int? = null

        viewModel.selectedAlertConfig.observe(this, Observer { receivedAlertConfig ->
            formerConfig = receivedAlertConfig
            formerDescription = receivedAlertConfig.config?.description
            formerState = receivedAlertConfig.inverse
            formerMaximum = receivedAlertConfig.maximum
            formerMinimum = receivedAlertConfig.minimum
        })

        condition_description_editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(textChanged: Editable?) {
                println(textChanged)
                if (!textChanged.isNullOrEmpty()) {
                    formerConfig?.config?.description = textChanged.toString()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        range_bar?.setOnThumbValueChangeListener { _, _, thumbIndex, value ->
            if (thumbIndex == 0) {
                range_bar_max?.text = value.toString()
                formerConfig?.minimum = value

            } else {
                range_bar_min?.text = value.toString()
                formerConfig?.maximum = value
            }
        }
        condition_triggered_switch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) condition_triggered_switch.text = getString(R.string.open_sensor)
            else condition_triggered_switch.text = getString(R.string.closed_sensor)
            formerConfig?.inverse = isChecked
        }

        validate_button.setOnClickListener {
            if (formerConfig?.config?.description != formerDescription) {
                viewModel.modifySensorDtDomainTypeDescription(formerConfig)
                condition_description_editText.text?.clear()
            }
            if (formerConfig?.inverse != formerState || formerConfig?.maximum != formerMaximum || formerConfig?.minimum != formerMinimum) {
                viewModel.changeConditionID(formerConfig)
            }
            Filter.followNewCondition(formerConfig)
            (activity as AlertConfigsActivity).onBackPressed()
            Handler().postDelayed({ viewModel.loadAllAlertConfigs() }, 500)
        }
    }

    private fun touchElementsVisibility() {
        alert_config_edition_container_temperature.visibility = View.INVISIBLE
        alert_config_edition_container_touch.visibility = View.VISIBLE
        alert_config_edition_container_proximity.visibility = View.INVISIBLE
    }

    private fun proximityElementsVisibility() {
        alert_config_edition_container_temperature.visibility = View.INVISIBLE
        alert_config_edition_container_touch.visibility = View.INVISIBLE
        alert_config_edition_container_proximity.visibility = View.VISIBLE
    }

    private fun temperatureElementsVisibility() {
        alert_config_edition_container_temperature.visibility = View.VISIBLE
        alert_config_edition_container_touch.visibility = View.INVISIBLE
        alert_config_edition_container_proximity.visibility = View.INVISIBLE
    }

    private fun expandableCardView() {
        cardView_edition_container.setOnClickListener {
            toggleEditionCardView()
        }
        cardView_history_container.setOnClickListener {
            toggleHistoryCardView()
        }
    }

    private fun expandHistoryCardView() {
        cardView_history_container.viewTreeObserver.addOnPreDrawListener(
            object : ViewTreeObserver.OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    cardView_history_container.viewTreeObserver.removeOnPreDrawListener(this)
                    compactHistoryHeight = cardView_history_container.height
                    if (alert_config_history_list_recycler_view.visibility == View.VISIBLE) {
                        if (expandedHistoryHeight == 0) expandedHistoryHeight = compactHistoryHeight * 7
                    } else {
                        if (expandedHistoryHeight == 0) expandedHistoryHeight = compactHistoryHeight * 3
                    }
                    return true
                }
            }
        )
    }

    private fun expandEditionCardView() {
        cardView_edition_container.viewTreeObserver.addOnPreDrawListener(
            object : ViewTreeObserver.OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    cardView_edition_container.viewTreeObserver.removeOnPreDrawListener(this)
                    compactEditionHeight = cardView_edition_container.height
                    if (expandedEditionHeight == 0) expandedEditionHeight = compactEditionHeight * 7
                    return true
                }
            })
    }

    private fun collapseEditionView() {
        val anim = ValueAnimator.ofInt(
            cardView_edition_container.measuredHeightAndState, compactEditionHeight
        )
        anim.addUpdateListener { valueAnimator ->
            val animatedValue = valueAnimator.animatedValue as Int
            val layoutParams = cardView_edition_container.layoutParams
            layoutParams.height = animatedValue
            cardView_edition_container.layoutParams = layoutParams
        }
        anim.start()
    }

    private fun collapseHistoryView() {
        val anim = ValueAnimator.ofInt(
            cardView_history_container.measuredHeightAndState, compactHistoryHeight
        )
        anim.addUpdateListener { valueAnimator ->
            val animatedValue = valueAnimator.animatedValue as Int
            val layoutParams = cardView_history_container.layoutParams
            layoutParams.height = animatedValue
            cardView_history_container.layoutParams = layoutParams
        }
        anim.start()
    }

    private fun expendEditionView(height: Int) {
        val anim = ValueAnimator.ofInt(cardView_edition_container.measuredHeightAndState, height)
        anim.addUpdateListener { valueAnimator ->
            val animatedValue = valueAnimator.animatedValue as Int
            val layoutParams = cardView_edition_container.layoutParams
            layoutParams.height = animatedValue
            cardView_edition_container.layoutParams = layoutParams
        }
        anim.start()
    }

    private fun expendHistoryView(height: Int) {
        val anim = ValueAnimator.ofInt(cardView_history_container.measuredHeightAndState, height)
        anim.addUpdateListener { valueAnimator ->
            val animatedValue = valueAnimator.animatedValue as Int
            val layoutParams = cardView_history_container.layoutParams
            layoutParams.height = animatedValue
            cardView_history_container.layoutParams = layoutParams
        }
        anim.start()
    }

    private fun toggleEditionCardView() {

        val rotationAnimator = ObjectAnimator.ofFloat(down_arrow_edition, getString(R.string.rotation), 0f, 180f)
        val rotationAnimatorReverse = ObjectAnimator.ofFloat(down_arrow_edition, getString(R.string.rotation), 180f, 0f)
        rotationAnimator.duration = rotationDuration
        rotationAnimatorReverse.duration = rotationDuration

        val animatorSet = AnimatorSet()
        animatorSet.play(rotationAnimator)
        val animatorSetReverse = AnimatorSet()
        animatorSetReverse.play(rotationAnimatorReverse)

        if (cardView_edition_container.height == compactEditionHeight) {
            alert_configuration_edition_container.visibility = View.VISIBLE
            expendEditionView(expandedEditionHeight)
            animatorSet.start()

        } else {
            alert_configuration_edition_container.visibility = View.GONE
            collapseEditionView()
            animatorSetReverse.start()
        }
    }

    private fun toggleHistoryCardView() {

        val rotationAnimator = ObjectAnimator.ofFloat(down_arrow_history, getString(R.string.rotation), 0f, 180f)
        val rotationAnimatorReverse = ObjectAnimator.ofFloat(down_arrow_history, getString(R.string.rotation), 180f, 0f)
        rotationAnimator.duration = rotationDuration
        rotationAnimatorReverse.duration = rotationDuration

        val animatorSet = AnimatorSet()
        animatorSet.play(rotationAnimator)
        val animatorSetReverse = AnimatorSet()
        animatorSetReverse.play(rotationAnimatorReverse)

        if (cardView_history_container.height == compactHistoryHeight) {
            alert_configuration_history_container.visibility = View.VISIBLE
            expendHistoryView(expandedHistoryHeight)
            animatorSet.start()
        } else {
            alert_configuration_history_container.visibility = View.GONE
            collapseHistoryView()
            animatorSetReverse.start()
        }
    }
}


