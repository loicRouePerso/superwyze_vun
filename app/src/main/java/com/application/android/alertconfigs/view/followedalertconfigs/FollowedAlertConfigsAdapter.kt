package com.application.android.alertconfigs.view.followedalertconfigs

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.drawable.GradientDrawable
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.application.android.R
import com.application.android.model.conditions.AlertConfig
import com.application.android.utils.DateConverter.convertMillisecondToDate

class FollowedAlertConfigsAdapter(private val followedAlertConfigList: MutableList<AlertConfig>) : RecyclerView.Adapter<FollowedAlertConfigsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_followed_alert_config, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return followedAlertConfigList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(followedAlertConfigList[position])
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        private val title = view.findViewById<TextView>(R.id.condition_title)
        private val location = view.findViewById<TextView>(R.id.condition_location_text)
        private val itemBackground = view.findViewById<ConstraintLayout>(R.id.item_condition_container)
        val image = view.findViewById<AppCompatImageView>(R.id.condition_image)
        private val date = view.findViewById<TextView>(R.id.followed_alert_config_triggered_date)
        private val textDate = view.findViewById<TextView>(R.id.followed_alert_config_activate_time)
        private val context: Context = image.context

        fun bind(alertConfigData: AlertConfig) {

            val jpg = alertConfigData.config?.type?.image
            val imageBytes = Base64.decode(jpg, 0)
            val imageBitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)

            title?.text = alertConfigData.config?.description
            location?.text = alertConfigData.config?.metadata
            image.setImageBitmap(imageBitmap)
//            val position = ViewHolder(itemView).adapterPosition
//            ViewCompat.setTransitionName(image,"transition$position" )
            val itemBackgroundColor = itemBackground.background as GradientDrawable
            val colorRed = ContextCompat.getColor(context, R.color.colorRed)
            val colorBlue = ContextCompat.getColor(context, R.color.colorPrimaryBlue)

             if (alertConfigData.timestamp != null) {
                 date?.text = convertMillisecondToDate(alertConfigData.timestamp)
                 itemBackgroundColor.setColor(colorRed)
             }else {
                 textDate.visibility = View.GONE
                 date.visibility = View.GONE
                 itemBackgroundColor.setColor(colorBlue)
             }
        }
    }
}
