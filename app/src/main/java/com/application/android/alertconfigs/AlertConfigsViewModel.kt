package com.application.android.alertconfigs

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.application.android.R
import com.application.android.api.RetrofitClientProvider
import com.application.android.app.AuthenticationPrefs
import com.application.android.app.Filter
import com.application.android.model.conditions.AlertConfig
import com.application.android.model.domainalerts.Data
import com.application.android.model.numberversion.NumberVersion
import com.application.android.utils.SingleLiveEvent
import com.onesignal.OneSignal
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class AlertConfigsViewModel : ViewModel() {

    private var recentlyDeletedItem: AlertConfig? = null
    private var recentlyDeletedItemPosition: Int = 0
    private lateinit var subscription: Disposable

    private val _loadingButtonVisibility = SingleLiveEvent<Any>()
    val loadingButtonVisibility: LiveData<Any>
        get() = _loadingButtonVisibility

    private val _disableLoadingButtonVisibility = SingleLiveEvent<Any>()
    val disableLoadingButtonVisibility: LiveData<Any>
        get() = _disableLoadingButtonVisibility

    private var allAlertConfigList: MutableList<AlertConfig> = arrayListOf()
    private var allHistoryList: MutableList<Data> = arrayListOf()
    private var allEstablishmentList: MutableList<String> = arrayListOf()
    private var allNumberVersion : MutableList<NumberVersion> = arrayListOf()

    private val playerId = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId
    private val token = AuthenticationPrefs.getAuthToken().toString()
    private val sentToken = "Bearer $token"

    private var displayFollowedAlertConfigList: MutableList<AlertConfig> = arrayListOf()
    private var displayUnfollowedAlertConfigList: MutableList<AlertConfig> = arrayListOf()


    private var allEstablishmentListMLD: MutableLiveData<MutableList<String>> = MutableLiveData()
    val displayedFollowedAlertConfigListMLD: MutableLiveData<MutableList<AlertConfig>> = MutableLiveData()
    val displayUnFollowedAlertConfigListMLD: MutableLiveData<MutableList<AlertConfig>> = MutableLiveData()
    val displayedHistoryListMLD: MutableLiveData<MutableList<Data>> = MutableLiveData()
    val numberVersionMLD : MutableLiveData<MutableList<NumberVersion>> = MutableLiveData()

    private val retrievingAlertConfigErrorMessage: MutableLiveData<Int> = MutableLiveData()
    val retrievingHistoryDataErrorMessage: MutableLiveData<Int> = MutableLiveData()
    val selectedAlertConfig = MutableLiveData<AlertConfig>()

    init {
        loadAllAlertConfigs()
        getNumberVersion()
    }

    fun loadAllAlertConfigs() {
        subscription = RetrofitClientProvider.provideSuperwyzeApi().getAllAlertConfigurations(sentToken, playerId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrieveAlertConfigStart() }
            .doOnTerminate { onRetrieveAlertConfigFinish() }
            .subscribe(
                { allConditionList ->
                    onRetrieveAlertConfigSuccess(allConditionList)
                },
                {
                    it.printStackTrace()
                    onRetrieveAlertConfigError()
                }
            )
    }

    fun changeConditionID(alertConfig: AlertConfig?) {
        subscription = RetrofitClientProvider.provideSuperwyzeApi()
            .putNewAlertConfigParameters(sentToken, playerId, alertConfig)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.computation())
            .doOnSubscribe { onRetrieveAlertConfigStart() }
            .doOnTerminate { onRetrieveAlertConfigFinish() }
            .subscribe(
                {
                    Filter.replaceConditionID(alertConfig, it)
                },
                {
                    it.printStackTrace()
                    onRetrieveAlertConfigError()
                }
            )
    }

    fun resetConditionTimestamp(alertConfig: AlertConfig) {
        subscription = RetrofitClientProvider.provideSuperwyzeApi()
            .resetCondition(sentToken, playerId, alertConfig.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.computation())
            .doOnSubscribe { onRetrieveAlertConfigStart() }
            .doOnTerminate { onRetrieveAlertConfigFinish() }
            .subscribe(
                {
                    loadAllAlertConfigs()
                },
                {
                    it.printStackTrace()
                    onRetrieveAlertConfigError()
                }
            )
    }

    private fun getNumberVersion(){
        subscription = RetrofitClientProvider.provideSuperwyzeApi().getNumberVersion()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrieveAlertConfigStart() }
            .doOnTerminate { onRetrieveAlertConfigFinish() }
            .subscribe(
                {
                    onRetrieveNumberVersion(it)
                },
                {
                    it.printStackTrace()
                    onRetrieveAlertConfigError()
                }
            )
    }

    fun modifySensorDtDomainTypeDescription(alertConfig: AlertConfig?) {
        subscription = RetrofitClientProvider.provideSuperwyzeApi()
            .modifySensorDtDomainType(sentToken, playerId, alertConfig?.config)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.computation())
            .doOnSubscribe { onRetrieveAlertConfigStart() }
            .doOnTerminate { onRetrieveAlertConfigFinish() }
            .subscribe(
                {},
                {
                    it.printStackTrace()
                    onRetrieveAlertConfigError()
                }
            )
    }

    fun getAllDomainAlerts() {
        subscription = RetrofitClientProvider.provideSuperwyzeApi()
            .getAllDomainAlerts(sentToken, playerId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.computation())
            .doOnSubscribe { onRetrieveHistoryDataStart() }
            .doOnTerminate { onRetrieveHistoryDataFinish() }
            .subscribe(
                {
                    onRetrieveHistoryInfoSuccess(it)
                },
                {
                    it.printStackTrace()
                    onRetrieveHistoryDataError()
                }
            )
    }

    fun applyFilter() {
        val triggered = Filter.getTriggeredOnlyFilterState()
        displayFollowedAlertConfigList.clear()
        displayUnfollowedAlertConfigList.clear()
        if (triggered) {
            allAlertConfigList.forEach { alertConfig ->
                if (alertConfig.timestamp != null && Filter.isFollowed(alertConfig)) {
                    displayFollowedAlertConfigList.add(alertConfig)
                } else if (!Filter.isFollowed(alertConfig)) {
                    displayUnfollowedAlertConfigList.add(alertConfig)
                }
            }
        } else {
            allAlertConfigList.forEach { condition ->
                if (Filter.isFollowed(condition)) {
                    displayFollowedAlertConfigList.add(condition)
                } else if (!Filter.isFollowed(condition)) {
                    displayUnfollowedAlertConfigList.add(condition)
                }
            }
        }
        displayedFollowedAlertConfigListMLD.postValue(displayFollowedAlertConfigList)
        displayUnFollowedAlertConfigListMLD.postValue(displayUnfollowedAlertConfigList)
    }

    fun applyTriggeredFilter() {
        displayedFollowedAlertConfigListMLD.postValue(displayFollowedAlertConfigList.sortedWith(compareBy { it.timestamp }) as MutableList<AlertConfig>?)
        applyFilter()
    }

    fun applyNameFilter() {
        displayedFollowedAlertConfigListMLD.postValue(displayFollowedAlertConfigList.sortedWith(compareBy { it.config?.description }) as MutableList<AlertConfig>?)
        applyFilter()
    }

    fun unFollowAlertConfig(position: Int) {
        recentlyDeletedItem = displayFollowedAlertConfigList[position]
        recentlyDeletedItemPosition = position
        recentlyDeletedItem?.let {
            Filter.unFollow(it)
        }
        applyFilter()
    }

    fun undoUnfollowCondition() {
        recentlyDeletedItem?.let {
            Filter.followNewCondition(it)
        }
        applyFilter()
    }

    private fun onRetrieveAlertConfigStart() {
        _loadingButtonVisibility.call()
        retrievingAlertConfigErrorMessage.value = null
    }

    private fun onRetrieveAlertConfigFinish() {
        _disableLoadingButtonVisibility.call()
    }

    private fun onRetrieveAlertConfigSuccess(conditionList: List<AlertConfig>) {
        allAlertConfigList.clear()
        allAlertConfigList.addAll(conditionList)
        allAlertConfigList.forEach { alertConfig ->
            alertConfig.config?.metadata?.let { establishmentName -> allEstablishmentList.add(establishmentName) }
        }
        allEstablishmentListMLD.postValue(allEstablishmentList)
        applyFilter()
    }

    private fun onRetrieveAlertConfigError() {
        retrievingAlertConfigErrorMessage.value = R.string.post_error
    }

    private fun onRetrieveHistoryDataStart() {
        _loadingButtonVisibility.call()
        retrievingHistoryDataErrorMessage.value = null
    }

    private fun onRetrieveHistoryDataFinish() {
        _disableLoadingButtonVisibility.call()
    }

    private fun onRetrieveHistoryInfoSuccess(historyList: List<Data>) {
        allHistoryList.clear()
        allHistoryList.addAll(historyList)
        displayedHistoryListMLD.postValue(allHistoryList.sortedWith(compareByDescending { it.timestamp }) as MutableList<Data>)
    }

    private fun onRetrieveNumberVersion(versionList : List<NumberVersion>){
        allNumberVersion.clear()
        allNumberVersion.addAll(versionList)
        numberVersionMLD.postValue(allNumberVersion)
    }

    private fun onRetrieveHistoryDataError() {
        retrievingHistoryDataErrorMessage.value = R.string.fetching_history_data_error
    }

    fun getUnfollowedCondition(position: Int): AlertConfig {
        return displayUnfollowedAlertConfigList[position]
    }

    fun getFollowedAlertConfig(position: Int): AlertConfig {
        return displayFollowedAlertConfigList[position]
    }

    fun selectAlertConfig(alertConfig: AlertConfig?) {
        selectedAlertConfig.value = alertConfig
    }

}


