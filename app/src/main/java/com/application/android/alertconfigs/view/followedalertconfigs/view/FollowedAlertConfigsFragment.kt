package com.application.android.alertconfigs.view.followedalertconfigs.view

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Color
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Handler
import android.view.*
import android.widget.RadioGroup
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.application.android.alertconfigs.AlertConfigsViewModel
import com.application.android.alertconfigs.view.followedalertconfigs.FollowedAlertConfigsAdapter
import com.application.android.app.Filter
import com.application.android.utils.swipe.SwipeHelperLeftSwipe
import com.application.android.model.conditions.AlertConfig
import com.application.android.utils.layout.OnItemClickListener
import com.application.android.utils.layout.addOnItemClickListener
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_followed_alert_configs.*
import com.application.android.R
import com.application.android.utils.OnTransitionCalled
import java.lang.ClassCastException

class FollowedAlertConfigsFragment : Fragment() {

    private var viewModel: AlertConfigsViewModel? = null
    private var errorSnackBar: Snackbar? = null
    private var compactHeight = 0
    internal var expandedHeight = 0
    private val errorClickListener = View.OnClickListener { viewModel?.loadAllAlertConfigs() }

    private lateinit var listener: OnTransitionCalled

    companion object {
        fun newInstance() = FollowedAlertConfigsFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_followed_alert_configs, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnTransitionCalled) listener = context
        else throw ClassCastException(context.toString())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()
        initFABAction()
        setLayout()
        setTriggeredState()
        displayLoading()
        observeError()
        expandableCardView()
        expandCardView()
        openAlertConfigurationFragment()
        leftSwipe()
        swipeRefresh()
        applyTriggeredOnlyFilter()
        applyNameFilters()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = activity?.run {
            ViewModelProviders.of(this).get(AlertConfigsViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeFollowedAlertConfigList()
    }

    private fun setLayout() {
        (alert_followed_alert_config_list_recycler_view.itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
        alert_followed_alert_config_list_recycler_view?.layoutManager = LinearLayoutManager(context)
    }

    private fun swipeRefresh() {
        followed_alert_config_list_swipe_refresh.setOnRefreshListener {
            scheduleRefreshStop()
            viewModel?.loadAllAlertConfigs()
        }
    }

    private fun initToolbar(){
        listener.initialize()
    }

    private fun initFABAction() {
        floating_button.setOnClickListener {
            listener.onFABAction()
        }
    }

    private fun scheduleRefreshStop() {
        val handler = Handler()
        handler.postDelayed({ followed_alert_config_list_swipe_refresh.isRefreshing = false }, 500)
    }

    private fun setTriggeredState() {
        val triggeredState = Filter.getTriggeredOnlyFilterState()
        triggered_only_followed_alert_config_switch.isChecked = triggeredState
    }

    private fun openAlertConfigurationFragment() {
        alert_followed_alert_config_list_recycler_view.addOnItemClickListener(object : OnItemClickListener {
            override fun onItemClicked(position: Int, view: View) {
                val holder = FollowedAlertConfigsAdapter.ViewHolder(view)
                val image = holder.image
                val alertConfig = viewModel?.getFollowedAlertConfig(position)
               // val alertConfigImage = alertConfig?.config?.type?.image
                listener.onFollowedAlertConfigSelected("followedAlertConfigList", image, alertConfig)
                viewModel?.selectAlertConfig(alertConfig)
                viewModel?.getAllDomainAlerts()
//             (activity as AlertConfigsActivity).initAlertConfigurationDisplay()
            }
        })
    }

    private fun applyNameFilters() {
        val radioGroup = view?.findViewById<RadioGroup>(R.id.filter_radio_group)
        radioGroup?.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.name_filter_radio_button -> {
                    viewModel?.applyNameFilter()
                }
                R.id.triggered_filter_radio_button -> {
                    viewModel?.applyTriggeredFilter()
                }
            }
        }
    }

    private fun applyTriggeredOnlyFilter() {
        triggered_only_followed_alert_config_switch.setOnCheckedChangeListener { _, isChecked ->
            Filter.saveTriggeredOnlyFilterState(isChecked)
            viewModel?.applyFilter()
        }
    }

    private fun observeFollowedAlertConfigList() {
        viewModel?.displayedFollowedAlertConfigListMLD?.observe(this, Observer { followedAlertConfigs ->
            displayList(followedAlertConfigs)
        })
    }

    private fun displayLoading() {
        followed_alert_config_list_progress_bar.visibility = View.VISIBLE
        alert_followed_alert_config_list_recycler_view.visibility = View.GONE
    }

    private fun leftSwipe() {
        context?.let {
            val colorRed = ContextCompat.getColor(it, R.color.colorRed)
            val swipeHelper: SwipeHelperLeftSwipe = object : SwipeHelperLeftSwipe(it, alert_followed_alert_config_list_recycler_view) {
                override fun instantiateUnderlayButton(viewHolder: RecyclerView.ViewHolder, underlayButtons: MutableList<UnderlayButton>) {
                    val position = viewHolder.adapterPosition
                    val alertConfig = viewModel?.getFollowedAlertConfig(position)
                    underlayButtons.add(UnderlayButton(it, "", R.drawable.ic_delete_white_24dp, colorRed, object : UnderlayButtonClickListener {
                        override fun onClick(pos: Int) {
                            val adapterPosition = viewHolder.adapterPosition
                            viewModel?.unFollowAlertConfig(adapterPosition)
                            val snackBar =
                                Snackbar.make(followed_alert_config_list_container, getString(R.string.removed_condition), Snackbar.LENGTH_LONG)
                            snackBar.setActionTextColor(Color.parseColor("#C51948"))
                            snackBar.setAction(getString(R.string.Cancel)) { viewModel?.undoUnfollowCondition() }
                            snackBar.show()
                        }
                    }
                    ))
                    if (alertConfig?.timestamp != null) {
                        underlayButtons.add(
                            UnderlayButton(
                                it,
                                getString(R.string.listen_condition),
                                R.drawable.ic_blank_24dp,
                                Color.parseColor("#8BC34A"),
                                object : UnderlayButtonClickListener {
                                    override fun onClick(pos: Int) {
                                        alertConfig.let { it1 -> viewModel?.resetConditionTimestamp(it1) }
                                    }
                                }
                            ))
                    }
                }
            }
            swipeHelper
        }
    }

    private fun displayList(conditionList: MutableList<AlertConfig>) {
        (alert_followed_alert_config_list_recycler_view.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        alert_followed_alert_config_list_recycler_view?.adapter = FollowedAlertConfigsAdapter(conditionList)

        viewModel?.loadingButtonVisibility?.observe(this, Observer {
            followed_alert_config_list_progress_bar.visibility = View.VISIBLE
            alert_followed_alert_config_list_recycler_view.visibility = View.GONE
        })

        viewModel?.disableLoadingButtonVisibility?.observe(this, Observer {
            followed_alert_config_list_progress_bar.visibility = View.GONE
            alert_followed_alert_config_list_recycler_view.visibility = View.VISIBLE
        })
        if (conditionList.isEmpty()) {
            alert_followed_alert_config_list_recycler_view.visibility = View.GONE
            empty_recycler_view_followed_alert_config_list_text_view.visibility = View.VISIBLE
        } else {
            alert_followed_alert_config_list_recycler_view.visibility = View.VISIBLE
            empty_recycler_view_followed_alert_config_list_text_view.visibility = View.GONE
        }
    }

    private fun observeError() {
        viewModel?.retrievingHistoryDataErrorMessage?.observe(this, Observer { errorMessage ->
            if (errorMessage != null) showError(errorMessage) else hideError()
        })
    }

    private fun showError(@StringRes errorMessage: Int) {
        errorSnackBar = Snackbar.make(fragment_container_followed, errorMessage, Snackbar.LENGTH_LONG)
        errorSnackBar?.setAction(R.string.retry, errorClickListener)
        errorSnackBar?.setActionTextColor(Color.RED)
        errorSnackBar?.show()
    }

    private fun hideError() {
        errorSnackBar?.dismiss()
    }

    private fun expandableCardView() {
        cardView.setOnClickListener {
            toggle()
        }
    }

    private fun expandCardView() {
        cardView.viewTreeObserver.addOnPreDrawListener(
            object : ViewTreeObserver.OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    cardView.viewTreeObserver.removeOnPreDrawListener(this)
                    compactHeight = cardView.height
                    if (expandedHeight == 0) expandedHeight = compactHeight * 6
                    return true
                }
            })
    }

    private fun collapseView() {
        val anim = ValueAnimator.ofInt(
            cardView.measuredHeightAndState, compactHeight
        )
        anim.addUpdateListener { valueAnimator ->
            val animatedValue = valueAnimator.animatedValue as Int
            val layoutParams = cardView.layoutParams
            layoutParams.height = animatedValue
            cardView.layoutParams = layoutParams

        }
        anim.start()
    }

    private fun expendView(height: Int) {
        val anim = ValueAnimator.ofInt(cardView.measuredHeightAndState, height)
        anim.addUpdateListener { valueAnimator ->
            val animatedValue = valueAnimator.animatedValue as Int
            val layoutParams = cardView.layoutParams
            layoutParams.height = animatedValue
            cardView.layoutParams = layoutParams
        }
        anim.start()
    }

    private fun toggle() {

        val positionAnimator = ValueAnimator.ofFloat(0f, compactHeight * 5.toFloat())
        val positionAnimatorReverse = ValueAnimator.ofFloat(compactHeight * 5.toFloat(), 0f)

        positionAnimator.addUpdateListener {
            val value = it.animatedValue as Float
            down_arrow.translationY = value
        }
        positionAnimatorReverse.addUpdateListener {
            val value = it.animatedValue as Float
            down_arrow.translationY = value
        }

        positionAnimator.duration = 300L
        positionAnimatorReverse.duration = 300L

        val rotationAnimator = ObjectAnimator.ofFloat(down_arrow, "rotation", 0f, 180f)
        val rotationAnimatorReverse = ObjectAnimator.ofFloat(down_arrow, "rotation", 180f, 0f)

        rotationAnimator.duration = 200L
        rotationAnimatorReverse.duration = 200L

        val animatorSet = AnimatorSet()
        animatorSet.play(positionAnimator).with(rotationAnimator)

        val animatorSetReverse = AnimatorSet()
        animatorSetReverse.play(positionAnimatorReverse).with(rotationAnimatorReverse)

        if (cardView.height == compactHeight) {
            filter_container.visibility = View.VISIBLE
            expendView(expandedHeight)
            animatorSet.start()

        } else {
            filter_container.visibility = View.GONE
            collapseView()
            animatorSetReverse.start()
        }
    }
}
