package com.application.android.app


import android.preference.PreferenceManager

object AuthenticationPrefs {

    private const val LOGIN = "LOGIN"
    private const val KEY_AUTH_TOKEN = "KEY_AUTH_TOKEN"
    private const val LOGGED_IN_PREF = "LOGGED_IN_PREF"

    private fun sharedPrefs() = PreferenceManager.getDefaultSharedPreferences(SuperwyzeApp.getAppContext())

    fun saveLogin(login: String) {
        val editor = sharedPrefs().edit()
        editor.putString(LOGIN, login).apply()
    }

    fun setLoggedIn() {
        val editor = sharedPrefs().edit()
        editor.putBoolean(LOGGED_IN_PREF, true)
        editor.apply()
    }

    fun setLoggedOut() {
        val editor = sharedPrefs().edit()
        editor.putBoolean(LOGGED_IN_PREF, false)
        editor.apply()
    }

    fun getLoggedStatus(): Boolean {
        return sharedPrefs().getBoolean(LOGGED_IN_PREF, false)
    }

    fun saveAuthToken(token: String) {
        val editor = sharedPrefs().edit()
        editor.putString(KEY_AUTH_TOKEN, token).apply()
    }

    fun getAuthToken(): String? {
        return sharedPrefs().getString(KEY_AUTH_TOKEN, "")
    }

}