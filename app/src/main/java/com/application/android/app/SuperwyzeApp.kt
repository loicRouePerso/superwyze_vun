package com.application.android.app

import android.app.Application
import android.content.Context
import com.application.android.alertconfigs.view.AlertConfigsActivity
import com.application.android.alertconfigs.view.followedalertconfigs.view.FollowedAlertConfigsFragment
import com.application.android.utils.notifications.NotificationOpenedHandler
import com.application.android.utils.notifications.NotificationReceivedHandler
import com.onesignal.OneSignal
import timber.log.Timber

class SuperwyzeApp : Application() {

    companion object {
        @get:Synchronized
        var instance: SuperwyzeApp? = null
            private set

        fun getAppContext(): Context? = instance?.applicationContext

        val TAG = SuperwyzeApp::class.java
            .simpleName
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        Timber.plant(Timber.DebugTree())

        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.InAppAlert)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .setNotificationReceivedHandler(NotificationReceivedHandler())
            .setNotificationOpenedHandler(NotificationOpenedHandler())
            .init()
    }

}
