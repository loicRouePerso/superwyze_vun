package com.application.android.app

import android.preference.PreferenceManager
import com.application.android.model.conditions.AlertConfig
import com.application.android.model.conditions.Config
import com.application.android.model.domainalerts.Data
import com.application.android.utils.DateConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object Filter {

    private const val KEY_FOLLOW_STATE = "KEY_FOLLOW_STATE"
    private const val KEY_TRIGGERED_FILTER_STATE = "KEY_TRIGGERED_FILTER_STATE"

    private var followed: MutableList<Int>? = null
    private val gson = Gson()
    private fun sharedPrefs() = PreferenceManager.getDefaultSharedPreferences(SuperwyzeApp.getAppContext())

    fun isFollowed(condition: AlertConfig): Boolean {
        return getFollowedConditions()?.contains(condition.id) == true
    }

    fun followNewCondition(conditionID: AlertConfig?) {
        val followed = getFollowedConditions()
        followed?.let {
            conditionID?.id?.let { it1 -> followed.add(it1) }
            saveFollowed(KEY_FOLLOW_STATE, followed)
        }
    }

    private fun getFollowedConditions(): MutableList<Int>? {
        if (followed == null) {
            val json = sharedPrefs().getString(KEY_FOLLOW_STATE, "")
            val type = object : TypeToken<MutableList<Int>>() {}.type
            followed = gson.fromJson<MutableList<Int>>(json, type) ?: return mutableListOf()
        }
        return followed
    }

    fun unFollow(condition: AlertConfig) {
        val followed = getFollowedConditions()
        followed?.let {
            followed.remove(condition.id)
            saveFollowed(KEY_FOLLOW_STATE, followed)
        }
    }

    private fun saveFollowed(key: String, list: List<Int>) {
        val json = gson.toJson(list)
        sharedPrefs().edit().putString(key, json).apply()
    }

    fun replaceConditionID(oldConditionID: AlertConfig?, newConditionID: AlertConfig) {
        val followed = getFollowedConditions()
        followed?.let {
            oldConditionID?.id?.let { it1 -> followed.remove(it1) }
            saveFollowed(KEY_FOLLOW_STATE, followed)
            followed.add(newConditionID.id)
            saveFollowed(KEY_FOLLOW_STATE, followed)
        }
    }

    fun saveTriggeredOnlyFilterState(switchState: Boolean) {
        val editor = sharedPrefs().edit()
        editor.putBoolean(KEY_TRIGGERED_FILTER_STATE, switchState)
        editor.apply()
    }

    fun getTriggeredOnlyFilterState(): Boolean {
        return sharedPrefs().getBoolean(KEY_TRIGGERED_FILTER_STATE, false)
    }

}